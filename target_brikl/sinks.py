"""Brikl target sink class, which handles writing streams."""

from __future__ import annotations

from target_brikl.client import BriklSink

class UpdateOrderMutationSink(BriklSink):
    name = "SalesOrders"

    @property
    def endpoint(self):
        return "/graphql/admin/public"

    def _get_shipping_status(self, status):
        status = status.lower()
        possible_status = {
            "shipped": "SHIPPED",
            "delivered": "DELIVERED",
            "cancelled": "CANCELLED",
            "exception": "PENDING",
            "on hold": "PENDING",
            "pending": "PENDING",
            "processing": "PENDING",
        }
        if status in possible_status.keys():
            return possible_status[status]
        
        return "PENDING"

    def preprocess_record(self, record, context):
        shipping_status = self._get_shipping_status(record["status"])
        record["id"] = self.search_for_existing_order(int(record["order_number"])).get('id')
        query = """
        mutation updateOrder($input: UpdateOrderInput!){
            updateOrder(input: $input) {
                order {
                    shippingStatus
                    notes {
                        id
                        note
                    }
                }
                orderId
            }
        }
        """
        query = {
            "query": query,
            "variables": {
                "input": {
                    "orderId": record["id"]
                }
            }
        }
        if record.get("shippingStatus"):
            query["variables"]["input"]["shippingStatus"] = shipping_status
        
        if record.get("tracking_number"):
            query["variables"]["input"]["notes"] = {"note": record["tracking_number"]}
        
        return query