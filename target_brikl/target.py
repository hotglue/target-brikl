"""Brikl target class."""

from __future__ import annotations

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_hotglue.target import TargetHotglue
from target_brikl.client import BriklSink
from target_brikl.sinks import (
    UpdateOrderMutationSink
)


class TargetBrikl(TargetHotglue):
    """Sample target for Brikl."""

    name = "target-brikl"
    _state = {}

    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            description="Your PAT for Brikl API",
        ),
        th.Property(
            "shop_id",
            th.StringType,
            description="Brikl Shop ID",
        ),
    ).to_dict()

    SINK_TYPES = [UpdateOrderMutationSink]


if __name__ == "__main__":
    TargetBrikl.cli()
