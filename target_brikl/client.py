"""Brikl target sink class, which handles writing streams."""

from __future__ import annotations

import backoff
import requests
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError

from singer_sdk.sinks import RecordSink


from target_brikl.auth import BriklAuthenticator
from target_hotglue.client import HotglueSink


class BriklSink(HotglueSink, RecordSink):
    """Brikl target sink class."""
    def __init__(
        self,
        target,
        stream_name,
        schema,
        key_properties,
    ):
        self._state = dict(target._state)
        super().__init__(target, stream_name, schema, key_properties)


    @property
    def authenticator(self):
        url = self.url()
        return BriklAuthenticator(self.config)

    @property
    def endpoint(self):
        return "/graphql/admin/public"

    @property
    def base_url(self) -> str:
        return "http://api.brikl.com"

    @property
    def unified_schema(self):
        return None


    @backoff.on_exception(
        backoff.expo,
        (RetriableAPIError, requests.exceptions.ReadTimeout),
        max_tries=5,
        factor=2,
    )
    def _request(
        self, http_method, endpoint, params=None, request_data=None, headers=None
    ) -> requests.PreparedRequest:
        """Prepare a request object."""
        url = self.url(endpoint)
        headers = self.http_headers
        headers.update(self.authenticator.auth_headers)

        response = requests.request(
            method=http_method,
            url=self.url(endpoint),
            params=params,
            headers=headers,
            json=request_data
        )
        self.validate_response(response)
        return response

    def search_for_existing_order(self, order_no):
        # Order ID mapping using order number
        query = """
        {
        orders{
            edges {
                node {
                    no
                    id
                    shippingStatus
                    notes {
                        id
                    note
                    }
                }
            }
        }}
        """
        query = {"query": query}
        resp = self._request(
            http_method="POST",
            endpoint=self.endpoint,
            request_data=query
        )
        resp = resp.json()
        if "errors" in resp:
            self.logger.error(f"{resp['errors']}")
            self.logger.error(f"Request sent: {query}")
            raise FatalAPIError(f"{resp['errors']}")
        
        for order in resp["data"]["orders"]["edges"]:
            if order["node"]["no"] == order_no:
                return order["node"]
        
        return None
        
    def preprocess_record(self, record: dict, context: dict) -> None:
        return record

    def process_record(self, record: dict, context: dict) -> None:
        # We are filtering for orders that have an order id defined
        # because we are not posting new orders through this API yet
        # this may change in the future
        if record["variables"]["input"]["orderId"] is None:
            return
        
        resp = self._request(
            http_method="POST",
            endpoint=self.endpoint,
            request_data=record
        )
        resp = resp.json()
        if "errors" in resp:
            if len(resp["errors"]) == 1 and "no properties were changed" in resp["errors"][0]["message"]:
                self.logger.info(f"Skipping record as nothing changed: {record}")
                return
            
            self.logger.error(f"{resp['errors']}")
            self.logger.error(f"Request sent: {record}")
            raise FatalAPIError(f"{resp['errors']}")
        
        self.logger.info(f"Record processed correctly: {resp}")