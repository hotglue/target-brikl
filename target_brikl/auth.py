import json
from datetime import datetime
from typing import Optional
from base64 import b64encode
from typing import Any, Dict, Optional

import logging
import requests


class BriklAuthenticator:
    """API Authenticator for OAuth 2.0 flows."""
    def __init__(self, config):
        self._config = config
    
    @property
    def auth_headers(self) -> dict:
        return {
            "Authorization": f"PAT {self._config.get('access_token')}",
            "x-brikl-shop-id": self._config.get('shop_id')
        }
        